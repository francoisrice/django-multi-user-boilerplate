## Django Multi User Application Base
### Get to building your Django app without building CRUD functions for users

#### Notes for Usage:
 - remove packages with `0.0.0` as their version, as they will cause error
 - change `django_multiuser` to the app's name in all files
 - generate a new secret key in `settings.py`

#### Current Dependencies:
 pip install 
  - django (web framework)
  - django-crispy-forms (forms styling)
  - Pillow (displaying images)


